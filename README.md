# build-environment

A collection of docker containers as a common CFS build environment on Linux.

Docker basics
--------------

To create a container, go to the directory and run
```
docker build -t <some-tag-name> .
```

You can then run the container by
```
docker run -ti <some-tag-name> bash
```
To have root priviliges inside the docker container (e.g. for testing package installation), use argument `-u root`.

To mount in the CFS source to `/cfs` in the container use the argument `-v ~/my/cfs/path:/cfs`.

It makes sense to store builds outside the container (otherwise they are destroyed when the container is quit).
Use the source dir from above or provide another host dir.

Container Images on a Container Registry
----------------------------------------

We use gitlab-CI to build container images and store them on the built in container registry.
See `.gitlab-ci.yml` for details.

Before you can push containers you need to [authenicate to the container registry](https://docs.gitlab.com/12.10/ee/user/packages/container_registry/#authenticating-to-the-gitlab-container-registry) e.g. by
```
docker login [OPTIONS] [SERVER]
```

We use [docker hub](https://hub.docker.com/) to publish our build containers.
They are collected in the organisation [openCFS](https://hub.docker.com/orgs/opencfs).
The containers can be built via gitlab-CI and are thus published automatically (username/password for login are set via *tokens* made available as *CI/CD variables*).
See `.gitlab-ci.yml` for details.

 
CFS build pipeline
-------------------

We use the docker containers in this repo as runners in the CFS build pipeline.
The pipeline [tags](https://docs.gitlab.com/ee/ci/yaml/#tags) define what a runner can do. We have
* `gccX` build with gcc version X 
* `release` has an old C-standard-lib such that it runs on all platforms (the `centos6` container)
* `linux` can run any `release` build
* `license-keys` has a `~/.cfs_platform_defaults.cmake` with the necessary information

One can define a [`pre_build_script`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section) for each runner that defines the build environment.
An example to add a runner using gcc7 via [software collections](https://www.softwarecollections.org/en/) is 
```
gitlab-runner register --url https://cfs-dev.mdmt.tuwien.ac.at/ -r Qa9xoypxMtu7dMWzU4xV --executor shell --pre-build-script "source /opt/rh/devtoolset-6/enable" --output-limit 12288 --tag-list "gcc6,linux,mdmt,git" --name pc11.mdmt_devtoolset-6
```
