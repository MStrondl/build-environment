FROM centos:7.9.2009

# install build dependencies for cfs
RUN yum install -y \ 
  make \
  patch \
  file \
  m4 \
  which \
  findutils \
  diffutils \
  # unzip only required for closed source cfsdeps
  unzip \
  git \
  --nogpgcheck

# install and setup recent cmake 
RUN yum install -y wget && wget -nv --no-check-certificate https://cmake.org/files/v3.15/cmake-3.15.7-Linux-x86_64.sh && sh cmake-3.15.7-Linux-x86_64.sh --prefix=/usr/local --skip-license

# install the intel oneAPI repo
# Intel oneAPI contains MKL, c++ and fortran compilers.
# install it from the [yum repos](https://software.intel.com/content/www/us/en/develop/articles/installing-intel-oneapi-toolkits-via-yum.html)
# first install the repo file (modified to disable gpg check)
COPY oneAPI.repo /etc/yum.repos.d/oneAPI.repo
# install MKL
RUN yum install -y intel-oneapi-mkl-devel-2021.2.0

# install python 3.6 via software-collections
RUN yum install -y centos-release-scl && yum install -y rh-python36
# install needed packages via pip
RUN source /opt/rh/rh-python36/enable && pip install --upgrade pip==21.2.4 && pip install lxml==4.6.3 scipy==1.5.4 numpy==1.19.5 matplotlib==3.3.4 h5py==3.1.0 vtk==9.0.1 pillow==8.3.2

# add a user in the container
RUN useradd -ms /bin/bash --uid 1000 gitlab-runner
USER gitlab-runner

WORKDIR /home/gitlab-runner

# start with a bash shell in devtoolset-4
ENTRYPOINT scl enable rh-python36 -- bash
